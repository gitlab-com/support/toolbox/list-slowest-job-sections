FROM rhub/r-minimal:latest

RUN apk update                                                                              \
    && apk --no-cache --update-cache add ripgrep                                            \
    && installr -d                                                                          \
       -t "musl binutils libgomp libatomic gmp isl26 mpfr4 mpc1 gcc musl-dev libc-dev g++"  \
       dplyr readr tidyr                                                                    \
    && mkdir -p /app/data                                                                   \
    && rm -rf /var/cache/apk/*

COPY ./list-slowest-job-sections   \
     ./list-slowest-job-sections.R \
     /app/

WORKDIR /app

CMD ["bash", "list-slowest-job-sections"]
