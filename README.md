# List slowest job sections

This script helps you find performance bottlenecks in
[CI jobs](https://docs.gitlab.com/ee/ci/jobs/#view-jobs-in-a-pipeline).
It extracts and processes the relevant data from the logs,
and calculates the duration of all sections within each job.
For a few jobs, this could also be done by reviewing
the white `mm:ss` badges in the GitLab job log UI.
However, a data analysis across many job logs may be better in some cases.

![demo](demo.gif)

## Usage

Running the script yields 2 outputs:

- `stdout` shows the slowest few job sections and some general advice.
- `list-of-job-sections.csv` contains all results for
   a possibly necessary, deeper analysis.

The [example data](./data/list-of-job-sections.csv)
is somewhat representative of a normal CI system.

### Docker

In the directory where your job `.log` or `.txt` files are, run:

```shell
docker run --rm -v ${PWD}:/app/data registry.gitlab.com/gitlab-com/support/toolbox/list-slowest-job-sections
# use :main or :latest tag
```

### Local installation

1. [the R programming language](https://www.r-project.org/)
    - `brew install --cask r` seems more compatible than
       [the formula](https://formulae.brew.sh/formula/r) :shrug:
1. [ripgrep](https://github.com/BurntSushi/ripgrep/blob/master/GUIDE.md),
   because `grep` does not re-emit ANSI codes from the raw job logs
1. `make install`
1. Place your raw job logs (`.txt` files) into the `input` directory.
1. Run `./list-slowest-job-sections`

Optionally, [RStudio Desktop](https://www.rstudio.com/products/rstudio/download/#download)
in case you want an R-specialized IDE.

## Contributing

MRs are welcome! For major changes, please open an issue first
to discuss what you would like to change.

## License: [GPL-3.0-or-later](https://spdx.org/licenses/GPL-3.0-or-later.html)
