CONTAINER := list-slowest-job-sections
IMAGE_TAG := $(CONTAINER):latest

all:
	docker buildx build --platform linux/amd64 --tag $(IMAGE_TAG) .

install:
	Rscript -e 'install.packages(c("dplyr", "readr", "tidyr"))'

clean:
	docker buildx prune
	docker container remove --force $(CONTAINER)

test:
	Rscript -e 'lintr::lint_dir()'
	checkmake Makefile
	hadolint Dockerfile

.PHONY: all clean test
